package com.lumos;

import com.lumos.configuration.LumosProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties({LumosProperties.class})
@ComponentScan
public class LumosApplication {

	public static void main(String[] args) {
		SpringApplication.run(LumosApplication.class, args);
	}
}
