package com.lumos.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devin on 6/8/17.
 */

@ConfigurationProperties(prefix = "lumos", ignoreUnknownFields = false)
public class LumosProperties {
    @Getter
    private List<String> frontEndUrls;

    public void setFrontEndUrls(String urls) {
        List<String> frontEndUrl = new ArrayList<>();
        for(String url: urls.split(",")) {
            frontEndUrl.add(url);
        }
        this.frontEndUrls = frontEndUrl;
    }
}
