package com.lumos.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by devin on 6/8/17.
 */
@Configuration
@Slf4j
public class WebConfig {

    private final Environment env;
    private final LumosProperties lumosProperties;

    public WebConfig(Environment env, LumosProperties lumosProperties) {
        this.env = env;
        this.lumosProperties = lumosProperties;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        for( String url: lumosProperties.getFrontEndUrls()) {
            config.addAllowedOrigin(url);
        }
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/api/**", config);
        return new CorsFilter(source);
    }

}

