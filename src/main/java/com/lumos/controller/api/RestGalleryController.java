package com.lumos.controller.api;

import com.lumos.data.dto.GalleryDto;
import com.lumos.service.GalleryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by devin on 3/18/17.
 */



@Slf4j
@RestController
public class RestGalleryController {

    @Autowired
    GalleryService galleryService;

    @RequestMapping(value = "/api/galleries", method = RequestMethod.GET)
    public HttpEntity<List<GalleryDto>> list() {
        List<GalleryDto> galleries = galleryService.list();
        return new ResponseEntity<>(galleries, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/galleries/{slug}", method = RequestMethod.GET)
    public HttpEntity<GalleryDto> find(@PathVariable String slug) {
        GalleryDto gallery = galleryService.findBySlug(slug);
        return new ResponseEntity<>(gallery, HttpStatus.OK);
    }
}
