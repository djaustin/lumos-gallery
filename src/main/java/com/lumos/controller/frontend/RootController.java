package com.lumos.controller.frontend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by devin on 3/18/17.
 */

@Controller
public class RootController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public HttpEntity<String> getFrontPage() {
        return new ResponseEntity<>("testing", HttpStatus.OK);
    }

    @RequestMapping(value = "/heartbeat", method = RequestMethod.GET)
    public HttpEntity<String> heartbeat() {
        return new ResponseEntity<String>("up", HttpStatus.OK);
    }
}
