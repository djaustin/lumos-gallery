package com.lumos.data.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by devin on 3/30/17.
 */


@Data
public class GalleryDto {
    private Integer id;
    private String name;
    private String description;
    private String slug;
    private Timestamp createdOn;
    private Timestamp updatedOn;
    private Integer imageCount;
    private String coverPhoto;
    private List<OriginalImageDto> originalImages;
}
