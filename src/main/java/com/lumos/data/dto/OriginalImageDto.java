package com.lumos.data.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * Created by devin on 3/30/17.
 */


@Data
public class OriginalImageDto {
    private Integer id;
    private String name;
    private Integer galleryId;
    private String description;
    private String path;
    private Integer height;
    private Integer width;
    private Integer filesize;
    private String slug;
    private Integer sortOrder;
    private String digest;
    private Timestamp createdOn;
    private Timestamp updatedOn;
}
