package com.lumos.data.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.NonNull;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by devin on 3/30/17.
 */

@Entity
public class Gallery {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column( name = "id" )
    private Integer id;

    @NonNull
    private String name;
    private String description;
    private String slug;
    private Timestamp createdOn;
    private Timestamp updatedOn;
    private String coverPhoto;

    @JsonBackReference
    @OneToMany(mappedBy = "gallery", cascade = CascadeType.ALL)
    private Set<OriginalImage> originalImages;

    @java.beans.ConstructorProperties({"id", "name", "description", "slug", "createdOn", "updatedOn", "originalImages", "coverPhoto"})
    public Gallery(Integer id, String name, String description, String slug, Timestamp createdOn, Timestamp updatedOn, Set<OriginalImage> originalImages, String coverPhoto) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.slug = slug;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.originalImages = originalImages;
        this.coverPhoto = coverPhoto;
    }

    public Gallery() {
    }

    public Integer getId() {
        return this.id;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getSlug() {
        return this.slug;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public Set<OriginalImage> getOriginalImages() {
        return this.originalImages;
    }

    public String getCoverPhoto() {
        return this.coverPhoto;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setOriginalImages(Set<OriginalImage> originalImages) {
        this.originalImages = originalImages;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String toString() {
        return "com.lumos.data.entity.Gallery(id=" + this.getId() + ")"; //, name=" + this.getName() + ", description=" + this.getDescription() + ", slug=" + this.getSlug() + ", createdOn=" + this.getCreatedOn() + ", updatedOn=" + this.getUpdatedOn() + ", originalImages=" + this.getOriginalImages() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Gallery)) return false;
        final Gallery other = (Gallery) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        final Object this$slug = this.getSlug();
        final Object other$slug = other.getSlug();
        if (this$slug == null ? other$slug != null : !this$slug.equals(other$slug)) return false;
        final Object this$createdOn = this.getCreatedOn();
        final Object other$createdOn = other.getCreatedOn();
        if (this$createdOn == null ? other$createdOn != null : !this$createdOn.equals(other$createdOn)) return false;
        final Object this$updatedOn = this.getUpdatedOn();
        final Object other$updatedOn = other.getUpdatedOn();
        if (this$updatedOn == null ? other$updatedOn != null : !this$updatedOn.equals(other$updatedOn)) return false;
        final Object this$coverPhoto = this.getCoverPhoto();
        final Object other$coverPhoto = other.getCoverPhoto();
        if (this$coverPhoto == null ? other$coverPhoto != null : !this$coverPhoto.equals(other$coverPhoto)) return false;
       // final Object this$originalImages = this.getOriginalImages();
       // final Object other$originalImages = other.getOriginalImages();
      //  if (this$originalImages == null ? other$originalImages != null : !this$originalImages.equals(other$originalImages))
       //     return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final Object $slug = this.getSlug();
        result = result * PRIME + ($slug == null ? 43 : $slug.hashCode());
        final Object $createdOn = this.getCreatedOn();
        result = result * PRIME + ($createdOn == null ? 43 : $createdOn.hashCode());
        final Object $updatedOn = this.getUpdatedOn();
        result = result * PRIME + ($updatedOn == null ? 43 : $updatedOn.hashCode());
        final Object $coverPhoto = this.getCoverPhoto();
        result = result * PRIME + ($coverPhoto == null ? 43 : $coverPhoto.hashCode());
        //final Object $originalImages = this.getOriginalImages();
      //  result = result * PRIME + ($originalImages == null ? 43 : $originalImages.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Gallery;
    }
}
