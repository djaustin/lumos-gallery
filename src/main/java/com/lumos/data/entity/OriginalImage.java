package com.lumos.data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.NonNull;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by devin on 3/30/17.
 */

@Entity
public class OriginalImage {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column( name = "id" )
    private Integer id;

    @JsonManagedReference
    @NonNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gallery_id")
    private Gallery gallery;

    @Column(name = "filename")
    private String filename;

    @NonNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;
    @Column(name = "path", nullable = false)
    private String path;
    @Column(name = "height")
    private Integer height;
    @Column(name = "width")
    private Integer width;
    @Column(name = "filesize")
    private Integer filesize;
    @Column(name = "slug")
    private String slug;
    @Column(name = "created_on")
    private Timestamp createdOn;
    @Column(name = "updated_on")
    private Timestamp updatedOn;
    @Column( name = "digest")
    private String digest;
    @Column( name = "sort_order")
    private Integer sortOrder;

    @java.beans.ConstructorProperties({"id", "gallery", "filename", "name", "description", "path", "height", "width", "filesize", "slug", "createdOn", "updatedOn", "digest", "sortOrder"})
    public OriginalImage(Integer id, Gallery gallery, String filename, String name, String description, String path, Integer height, Integer width, Integer filesize, String slug, Timestamp createdOn, Timestamp updatedOn, String digest, Integer sortOrder) {
        this.id = id;
        this.gallery = gallery;
        this.filename = filename;
        this.name = name;
        this.description = description;
        this.path = path;
        this.height = height;
        this.width = width;
        this.filesize = filesize;
        this.slug = slug;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.digest = digest;
        this.sortOrder = sortOrder;
    }

    public OriginalImage() {
    }

    public Integer getId() {
        return this.id;
    }

    @NonNull
    public Gallery getGallery() {
        return this.gallery;
    }

    @NonNull
    public String getFilename() {
        return this.filename;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getPath() {
        return this.path;
    }

    public Integer getHeight() {
        return this.height;
    }

    public Integer getWidth() {
        return this.width;
    }

    public Integer getFilesize() {
        return this.filesize;
    }

    public String getSlug() {
        return this.slug;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public String getDigest() {
        return this.digest;
    }

    public Integer getSortOrder() {
        return this.sortOrder;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setGallery(@NonNull Gallery gallery) {
        this.gallery = gallery;
    }

    public void setFilename(@NonNull String filename) {
        this.filename = filename;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public void setFilesize(Integer filesize) {
        this.filesize = filesize;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String toString() {
        return "com.lumos.data.entity.OriginalImage(id=" + this.getId() + ")";//, gallery=" + this.getGallery() + ", filename=" + this.getFilename() + ", name=" + this.getName() + ", description=" + this.getDescription() + ", path=" + this.getPath() + ", height=" + this.getHeight() + ", width=" + this.getWidth() + ", filesize=" + this.getFilesize() + ", slug=" + this.getSlug() + ", createdOn=" + this.getCreatedOn() + ", updatedOn=" + this.getUpdatedOn() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof OriginalImage)) return false;
        final OriginalImage other = (OriginalImage) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        final Object this$gallery = this.getGallery();
        final Object other$gallery = other.getGallery();
        if (this$gallery == null ? other$gallery != null : !this$gallery.equals(other$gallery)) return false;
        final Object this$filename = this.getFilename();
        final Object other$filename = other.getFilename();
        if (this$filename == null ? other$filename != null : !this$filename.equals(other$filename)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        final Object this$location = this.getPath();
        final Object other$location = other.getPath();
        if (this$location == null ? other$location != null : !this$location.equals(other$location)) return false;
        final Object this$height = this.getHeight();
        final Object other$height = other.getHeight();
        if (this$height == null ? other$height != null : !this$height.equals(other$height)) return false;
        final Object this$width = this.getWidth();
        final Object other$width = other.getWidth();
        if (this$width == null ? other$width != null : !this$width.equals(other$width)) return false;
        final Object this$filesize = this.getFilesize();
        final Object other$filesize = other.getFilesize();
        if (this$filesize == null ? other$filesize != null : !this$filesize.equals(other$filesize)) return false;
        final Object this$slug = this.getSlug();
        final Object other$slug = other.getSlug();
        if (this$slug == null ? other$slug != null : !this$slug.equals(other$slug)) return false;
        final Object this$createdOn = this.getCreatedOn();
        final Object other$createdOn = other.getCreatedOn();
        if (this$createdOn == null ? other$createdOn != null : !this$createdOn.equals(other$createdOn)) return false;
        final Object this$updatedOn = this.getUpdatedOn();
        final Object other$updatedOn = other.getUpdatedOn();
        if (this$updatedOn == null ? other$updatedOn != null : !this$updatedOn.equals(other$updatedOn)) return false;
        final Object this$digest = this.getDigest();
        final Object other$digest = other.getDigest();
        if (this$digest == null ? other$digest != null : !this$digest.equals(other$digest)) return false;
        final Object this$sortOrder = this.getSortOrder();
        final Object other$sortOrder = other.getSortOrder();
        if (this$sortOrder == null ? other$sortOrder != null : !this$sortOrder.equals(other$sortOrder)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final Object $gallery = this.getGallery();
        result = result * PRIME + ($gallery == null ? 43 : $gallery.hashCode());
        final Object $filename = this.getFilename();
        result = result * PRIME + ($filename == null ? 43 : $filename.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final Object $location = this.getPath();
        result = result * PRIME + ($location == null ? 43 : $location.hashCode());
        final Object $height = this.getHeight();
        result = result * PRIME + ($height == null ? 43 : $height.hashCode());
        final Object $width = this.getWidth();
        result = result * PRIME + ($width == null ? 43 : $width.hashCode());
        final Object $filesize = this.getFilesize();
        result = result * PRIME + ($filesize == null ? 43 : $filesize.hashCode());
        final Object $slug = this.getSlug();
        result = result * PRIME + ($slug == null ? 43 : $slug.hashCode());
        final Object $createdOn = this.getCreatedOn();
        result = result * PRIME + ($createdOn == null ? 43 : $createdOn.hashCode());
        final Object $updatedOn = this.getUpdatedOn();
        result = result * PRIME + ($updatedOn == null ? 43 : $updatedOn.hashCode());
        final Object $digest = this.getDigest();
        result = result * PRIME + ($digest == null ? 43 : $digest.hashCode());
        final Object $sortOrder = this.getSortOrder();
        result = result * PRIME + ($sortOrder == null ? 43 : $sortOrder.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof OriginalImage;
    }
}
