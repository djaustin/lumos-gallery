package com.lumos.data.repository;

import com.lumos.data.dto.GalleryDto;
import com.lumos.data.dto.OriginalImageDto;
import com.lumos.data.entity.Gallery;
import com.lumos.data.entity.OriginalImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by devin on 3/30/17.
 */

@Repository
public interface GalleryRepository extends JpaRepository<Gallery, Long> {

    @Query(value = "SELECT g.*, o.* FROM gallery g LEFT JOIN original_image o on o.gallery_id=g.id WHERE g.slug=?1 ORDER BY o.sort_order ASC", nativeQuery = true)
    Gallery findBySlug(String slug);

    @Query(value = "SELECT * FROM gallery ORDER BY created_on DESC", nativeQuery = true)
    List<Gallery> sortedByCreatedOn();

    List<OriginalImage> findByOriginalImages(String slug);

}
