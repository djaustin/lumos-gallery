package com.lumos.data.repository;

import com.lumos.data.entity.OriginalImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by devin on 3/30/17.
 */

@Repository
public interface OriginalImageRepository extends JpaRepository<OriginalImage, Long> {

    OriginalImage findBySlug(String slug);

}
