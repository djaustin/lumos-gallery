package com.lumos.service;

import com.lumos.data.dto.OriginalImageDto;
import com.lumos.data.entity.Gallery;
import com.lumos.data.repository.GalleryRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import com.lumos.data.dto.GalleryDto;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by devin on 3/30/17.
 */

@Service
public class GalleryService {
    private GalleryRepository galleryRepository;

    public GalleryService(GalleryRepository galleryRepository) {
        this.galleryRepository = galleryRepository;
    }

    public GalleryDto findBySlug(String slug) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(galleryRepository.findBySlug(slug), GalleryDto.class);
    }

    // TODO: Pagination
    public List<GalleryDto> list() {
        ModelMapper mapper = new ModelMapper();
        Type listType = new TypeToken<List<GalleryDto>>(){}.getType();
        List<Gallery> galleries = galleryRepository.sortedByCreatedOn();
        return mapper.map(galleries, listType);
    }

}
