ALTER TABLE gallery ADD COLUMN slug TEXT;
CREATE UNIQUE INDEX gallery_slug_idx ON gallery(slug);
