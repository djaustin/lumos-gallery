ALTER TABLE original_image ADD COLUMN slug TEXT;
CREATE UNIQUE INDEX original_image_slug_idx ON original_image(slug);