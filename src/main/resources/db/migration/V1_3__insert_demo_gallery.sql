INSERT INTO gallery(name, description, slug) VALUES ('Demo', 'Demo Gallery', 'demo-gallery');

INSERT INTO original_image(gallery_id, filename, name, location, slug) VALUES((SELECT id FROM gallery WHERE name = 'Demo'), 'test.jpg', 'test image', '/test.jpg', 'test-image');