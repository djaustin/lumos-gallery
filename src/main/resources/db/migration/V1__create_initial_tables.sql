create table gallery (
  id serial not null primary key,
  name text not null,
  description text,
  created_on timestamp not null default now(),
  updated_on timestamp not null default now()
);

create table original_image (
  id serial not null primary key,
  gallery_id int references gallery(id),
  filename text not null,
  name text not null,
  description text,
  location text not null,
  height int,
  width int,
  filesize int,
  created_on timestamp not null default now(),
  updated_on timestamp not null default now()
);